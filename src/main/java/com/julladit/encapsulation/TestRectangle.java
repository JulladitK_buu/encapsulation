/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.encapsulation;

/**
 *
 * @author acer
 */
public class TestRectangle {

    public static void main(String[] args) {
        Rectangle rec1 = new Rectangle(3, 2);
        System.out.println("Area of Rectangle1(r = " + rec1.getW() + "," + rec1.getH() + ") is " + rec1.calArea());
        rec1.setHW(4,6);
        System.out.println("Area of Rectangle1(r = " + rec1.getW() + "," + rec1.getH() + ") is " + rec1.calArea());
        rec1.setHW(0,0) ;
        System.out.println("Area of Rectangle1(r = " + rec1.getW() + "," + rec1.getH() + ") is " + rec1.calArea());
    
    }
}
