/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.encapsulation;

/**
 *
 * @author acer
 */
public class Square {

    private int A;

    public Square(int A) {
        this.A = A;
    }

    public int calArea() {
        return A * A;
    }

    public int getA() {
        return A;
    }

    public void setA(int A) {
        if (A <= 0) {
            System.out.println("Error: Radius must more than zero!!!!");
            return;
        }
        this.A = A;
    }

}
