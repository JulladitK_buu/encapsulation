/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.encapsulation;

/**
 *
 * @author acer
 */
public class Triangle {

    private double F;
    private double Y;
    static final double B = 0.5;

    public Triangle(double F, double Y) {
        this.F = F;
        this.Y = Y;
    }

    public double calArea() {
        return B * F * Y;
    }

    public double getF() {
        return F;
    }

    public double getY() {
        return Y;
    }

    public void setR(double F ,double Y) {
        if (F <= 0 || Y <= 0) {
            System.out.println("Error: Radius must more than zero!!!!");
            return;
        }
        this.F = F;
        this.Y = Y;

    }
}
