/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.encapsulation;

/**
 *
 * @author acer
 */
public class TestTriangle {

    public static void main(String[] args) {

        Triangle tri1 = new Triangle(5, 6);
        System.out.println("Area of Triangle(r = " + tri1.getF() + "," + tri1.getY() + ") is " + tri1.calArea());
        tri1.setR(7, 6);
        System.out.println("Area of Triangle(r = " + tri1.getF() + "," + tri1.getY() + ") is " + tri1.calArea());
        tri1.setR(0, 0);
        System.out.println("Area of Triangle(r = " + tri1.getF() + "," + tri1.getY() + ") is " + tri1.calArea());
    }
}
