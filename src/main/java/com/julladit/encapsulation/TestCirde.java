/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.encapsulation;

/**
 *
 * @author acer
 */
public class TestCirde {

    public static void main(String[] args) {
        Cirde Cirlde1 = new Cirde(3);
        System.out.println("Area of Cirlde1(r = " + Cirlde1.getR() + ") is " + Cirlde1.calArea());
        Cirlde1.r = 2; //cirlde1,r = 2;  
        System.out.println("Area of Cirlde1(r = " + Cirlde1.getR() + ") is " + Cirlde1.calArea());
        Cirlde1.setR(0);
        System.out.println("Area of Cirlde1(r = " + Cirlde1.getR() + ") is " + Cirlde1.calArea());

    }

}
