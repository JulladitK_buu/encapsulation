/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.encapsulation;

/**
 *
 * @author acer
 */
public class Rectangle {

    private int h;
    private int w;

    public Rectangle(int h, int w) {
        this.h = h;
        this.w = w;
    }

    public int calArea() {
        return h * w;

    }

    public int getH() {
        return h;
    }
    public int getW(){
        return w;
    }
    public void setHW(int h,int w) {
        if (h <= 0 && w <= 0) {
            System.out.println("Error: Radius must more than zero!!!!");
            return;
        }
        this.h = h;
        this.w = w;

    }
}
