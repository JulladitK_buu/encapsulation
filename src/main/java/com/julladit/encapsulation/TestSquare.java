/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.encapsulation;

/**
 *
 * @author acer
 */
public class TestSquare {

    public static void main(String[] args) {
        Square squ1 = new Square(3);
        System.out.println("Area of Square(r = " + squ1.getA() + ") is " + squ1.calArea());
        squ1.setA(5);
        System.out.println("Area of Square(r = " + squ1.getA() + ") is " + squ1.calArea());
        squ1.setA(0);
        System.out.println("Area of Square(r = " + squ1.getA() + ") is " + squ1.calArea());
    }
}
